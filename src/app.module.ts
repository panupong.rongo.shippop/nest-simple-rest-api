import {
  Module,
  NestModule,
  MiddlewareConsumer,
  RequestMethod,
} from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { CatsModule } from './cats/cats.module'
import configuration from './config/configuration'
import { LoggerClassMiddleware } from './common/middleware/logger.class.middleware'
import { LoggerFuncMiddleware } from './common/middleware/logger.func.middleware'

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env.local', '.env'], // ตั้งค่าตัวแปร env file path ที่จะอ่าน
      load: [configuration],
      isGlobal: true,
    }),
    CatsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerClassMiddleware, LoggerFuncMiddleware)
      .forRoutes({ path: 'cats', method: RequestMethod.ALL })
  }
}
