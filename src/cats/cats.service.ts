import { Injectable } from '@nestjs/common'
import { Cat } from './interfaces/cat.interface'
import { CreateCatDto, ListAllEntities, UpdateCatDto } from './dto'

@Injectable()
export class CatsService {
  private readonly cats: Cat[] = [
    { id: 1, name: 'Mocha The Angry Cat', age: 4, breed: 'Thai' },
    { id: 2, name: 'Latte The Fat Cat', age: 4, breed: 'Thai' },
  ]

  private async findMyCat(id: number): Promise<number | null> {
    const whereIsCat: number = this.cats.findIndex((c) => {
      return String(c.id) === String(id)
    })
    if (whereIsCat === -1) {
      return null
    }
    return whereIsCat
  }

  async create(cat: CreateCatDto): Promise<Cat> {
    const lastIndex: number = this.cats.length
    const currentCat: Cat = { ...cat, id: lastIndex + 1 }
    this.cats.push(currentCat)
    return currentCat
  }

  async getAll(query: ListAllEntities): Promise<Cat[]> {
    const { limit } = query
    const selectedCat: Cat[] = this.cats.slice(0, limit)
    return selectedCat
  }

  async getById(id: number): Promise<Cat> | null {
    const whereIsCat: number = await this.findMyCat(id)
    if (whereIsCat === -1) {
      return null
    }
    return this.cats[whereIsCat]
  }

  async updateById(id: number, body: UpdateCatDto): Promise<Cat> | null {
    const whereIsCat: number = await this.findMyCat(id)
    if (whereIsCat === -1) {
      return null
    }
    const targetCat: Cat = this.cats[whereIsCat]

    const updateCat: Cat = { ...body, id: targetCat.id }

    this.cats[whereIsCat] = updateCat

    return updateCat
  }

  async deleteById(id: number): Promise<Cat> | null {
    const whereIsCat: number = await this.findMyCat(id)
    if (whereIsCat === -1) {
      return null
    }

    const deleteCat: Cat = this.cats[whereIsCat]

    this.cats.splice(whereIsCat, 1)

    return deleteCat
  }
}
