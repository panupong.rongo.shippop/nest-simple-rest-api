import {
  Controller,
  Param,
  Body,
  Query,
  Get,
  Post,
  Patch,
  Delete,
} from '@nestjs/common'

import { CatsService } from './cats.service'
import { CreateCatDto, ListAllEntities, UpdateCatDto } from './dto'
import { Cat } from './interfaces/cat.interface'

@Controller('cats')
export class CatsController {
  constructor(private readonly catsService: CatsService) {}

  @Post()
  async create(@Body() createCatDto: CreateCatDto): Promise<Cat | null> {
    // return 'This action adds a new cat'
    const newCat = await this.catsService.create(createCatDto)
    return newCat
  }

  @Get()
  async findAll(@Query() query: ListAllEntities): Promise<Cat[] | []> {
    const cats = await this.catsService.getAll(query)
    return cats
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<Cat | null> {
    const cat = await this.catsService.getById(id)
    return cat
  }

  @Patch(':id')
  async update(
    @Param('id') id: number,
    @Body() updateCatDto: UpdateCatDto,
  ): Promise<Cat | null> {
    const updatedCat = await this.catsService.updateById(id, updateCatDto)
    return updatedCat
  }

  @Delete(':id')
  async remove(@Param('id') id: number): Promise<Cat | null> {
    const deletedCat = await this.catsService.deleteById(id)
    return deletedCat
  }
}
