import { Request, Response, NextFunction } from 'express'

export function LoggerFuncMiddleware(
  req: Request,
  res: Response,
  next: NextFunction,
) {
  console.info(`Request func ...`)
  next()
}
